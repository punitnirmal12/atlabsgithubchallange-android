package com.github.demo.model

data class UserRepoDetails(
    val id: Int,
    val node_id: String,
    val name: String,
    val full_name: String,
    val private: Boolean,
    val owner: Owner? = null,
    val created_at: String? = "NA",
    val updated_at: String? = "NA",
    val pushed_at: String? = "NA",
    val git_url: String = "NA",
    val language: String?,
    val description: String?,
    val open_issues_count: Int = 0,
    val html_url: String
)
