package com.github.demo.base

import androidx.recyclerview.widget.RecyclerView
/*Base adapter class*/
abstract class BaseRecyclerViewAdapter<V : BaseViewHolder> : RecyclerView.Adapter<V>(),
    BaseAdapterItemListener {

    override fun onBindViewHolder(holder: V, position: Int) {
        holder.setListenerCallback(this)
        bindHolder(holder, position, null)
    }

    override fun onBindViewHolder(holder: V, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        holder.setListenerCallback(this)
        bindHolder(holder, position, payloads)
    }

    abstract fun bindHolder(holder: V, position: Int, payloads: MutableList<Any>?)

}