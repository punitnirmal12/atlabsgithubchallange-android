package com.github.demo.base

/*Base view class used for setup required things*/
interface BaseView<T> {
    /*to perform intial operations*/
    fun init()

    /*display progressbar*/
    fun showProgress()

    /*hide progressbar*/
    fun hideProgress()

    /*set data in view*/
    fun setData(datas: T)
}