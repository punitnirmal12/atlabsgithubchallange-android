package com.github.demo.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    protected var listener: BaseAdapterItemListener? = null

    init {
        itemView.setOnClickListener {
            listener?.itemClicked(it.id, adapterPosition)
        }
    }

    fun setListenerCallback(listener: BaseAdapterItemListener){
        this.listener = listener
    }
}