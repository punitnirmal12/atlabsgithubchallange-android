package com.github.demo.base

import android.os.Bundle

/*list item click listener*/
interface BaseAdapterItemListener {
    fun itemClicked(id: Int, position: Int, bundle: Bundle? = null)
}