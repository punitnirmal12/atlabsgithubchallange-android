package com.github.demo.webservice

import com.github.demo.model.SearchResult
import com.github.demo.model.UserRepoDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/* Retrofit interfaces*/
interface APIInterface {

    @GET("users/{user}")
    fun getUserDetails(@Path("user") user: String?): Call<SearchResult>

    @GET("users/{user}/repos")
    fun getUserRepoDetails(@Path("user") user: String?): Call<List<UserRepoDetails>>
}