package com.github.demo.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.github.demo.utils.Utils


class InternetReceiver(private var mCallback: OnNetworkStatusReceiver) : BroadcastReceiver() {


    override fun onReceive(context: Context, intent: Intent?) {
        var status: String? = Utils.getConnectivityStatusString(context);
        if (status != null) {
            if (status.isEmpty() || status == "No internet is available" || status == "No Internet Connection") {
                mCallback.networkStatusReceiver(false)
            } else {
                mCallback.networkStatusReceiver(true)
            }
        }
    }

    interface OnNetworkStatusReceiver {
        fun networkStatusReceiver(data: Boolean)
    }
}