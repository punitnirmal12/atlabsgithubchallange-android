package com.github.demo.ui.search

import com.github.demo.base.BaseView

interface SearchView<T> : BaseView<T> {

    /*call search API method*/
    fun callSearchAPI(name: String)

    /*open details page method*/
    fun openDetailsPage()
}