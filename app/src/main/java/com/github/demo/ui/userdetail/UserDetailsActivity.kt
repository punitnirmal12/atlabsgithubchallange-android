package com.github.demo.ui.userdetail

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.github.demo.R
import com.github.demo.broadcastreceiver.InternetReceiver
import com.github.demo.databinding.ActivityMainBinding
import com.github.demo.databinding.ActivityRepoDetailsBinding
import com.github.demo.model.SearchResult
import com.github.demo.model.UserRepoDetails
import com.github.demo.utils.Utils
import com.github.demo.webservice.APIInterface
import com.github.demo.webservice.RetrofitClient
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_repo_details.*
import kotlinx.android.synthetic.main.custom_layout_internet_connection.*
import kotlinx.android.synthetic.main.custom_progress_bar_view.*
import kotlinx.android.synthetic.main.item_layout_user_details.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UserDetailsActivity : AppCompatActivity(), RepoView<List<UserRepoDetails>>,
    InternetReceiver.OnNetworkStatusReceiver {

    /*define variables here*/
    private var TAG = UserDetailsActivity::class.java.canonicalName
    private lateinit var mSearchData: SearchResult
    private lateinit var mAdapter: RepoAdapter
    private lateinit var MyReceiver: InternetReceiver
    private lateinit var mFilter: IntentFilter
    lateinit var mBindings: ActivityRepoDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBindings = DataBindingUtil.setContentView(this, R.layout.activity_repo_details)
        init()
    }

    /* init view related logic here*/
    override fun init() {
        setSupportActionBar(toolbar)
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar()?.setDisplayShowHomeEnabled(true);
        }
        MyReceiver = InternetReceiver(this)
        registerReceiver()
        val gson = Gson()
        mSearchData = gson.fromJson(intent.getStringExtra("data"), SearchResult::class.java)
        callRepoListWebservice(mSearchData.login)
    }

    /*Register internet receiver*/
    private fun registerReceiver() {
        mFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(MyReceiver, mFilter);
    }

    override fun onResume() {
        super.onResume()
        registerReceiver()
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(MyReceiver);
    }

    /*Used for show progress*/
    override fun showProgress() {
        rlProgressBarView.visibility = View.VISIBLE
    }

    /*Used for hide progressbar*/
    override fun hideProgress() {
        rlProgressBarView.visibility = View.GONE
    }

    /*set data into views*/
    override fun setData(datas: List<UserRepoDetails>) {
        txtDescriptionDetails.setText(mSearchData.bio)
        txtRepoNameDetails.setText(mSearchData.login)
        txtNameDetails.setText(mSearchData.name)
        txtFollowingsDetails.setText("" + mSearchData.following)
        txtFollowersDetails.setText("" + mSearchData.followers)
        txtPublicCountDetails.setText("" + mSearchData.public_repos)
        rvRepoList.layoutManager = LinearLayoutManager(this)
        mAdapter = RepoAdapter(this, datas)
        rvRepoList.adapter = mAdapter
        Glide.with(this)
            .load(mSearchData.avatar_url)
            .centerCrop()
            .placeholder(R.drawable.ic_github)
            .error(R.drawable.ic_github)
            .fallback(R.drawable.ic_github)
            .into(imgUserDetails)
        imgUserDetails.setOnClickListener(View.OnClickListener {
            Utils.loadURLinBrowser(this@UserDetailsActivity, mSearchData.html_url)
        })
    }

    /*Call web service for get repo list of user*/
    override fun callRepoListWebservice(name: String) {
        if (Utils.isOnline(this)) {
            showProgress()
            val apiInterface = RetrofitClient.getClient().create(APIInterface::class.java)
            val call = apiInterface.getUserRepoDetails(name)
            call.enqueue(object : Callback<List<UserRepoDetails>> {
                /*handle failure responce*/
                override fun onFailure(call: Call<List<UserRepoDetails>>, t: Throwable) {
                    Utils.displayToast(
                        this@UserDetailsActivity,
                        getString(R.string.something_went_wrong)
                    )
                    this@UserDetailsActivity.hideProgress()
                }

                /*handle success response*/
                override fun onResponse(
                    call: Call<List<UserRepoDetails>>,
                    response: Response<List<UserRepoDetails>>
                ) {
                    // check data valid or not
                    if (response.body() != null) {
                        setData(response.body()!!)
                    } else {
                        Utils.displayToast(
                            this@UserDetailsActivity,
                            getString(R.string.something_went_wrong)
                        )
                    }
                    this@UserDetailsActivity.hideProgress()
                }
            })
        }
    }

    /*Internet Connection Listener*/
    override fun networkStatusReceiver(data: Boolean) {
        if (data) {
            rlInternetConnection.visibility = View.GONE
        } else {
            rlInternetConnection.visibility = View.VISIBLE
        }
    }

    /*handle option menu click events.*/
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish() // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item)
    }
}