package com.github.demo.ui.search

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.github.demo.R
import com.github.demo.broadcastreceiver.InternetReceiver
import com.github.demo.databinding.ActivityMainBinding
import com.github.demo.model.SearchResult
import com.github.demo.ui.userdetail.UserDetailsActivity
import com.github.demo.utils.Utils
import com.github.demo.utils.ValidationUtils
import com.github.demo.webservice.APIInterface
import com.github.demo.webservice.RetrofitClient
import com.google.gson.Gson
import kotlinx.android.synthetic.main.custom_layout_internet_connection.*
import kotlinx.android.synthetic.main.custom_progress_bar_view.*
import kotlinx.android.synthetic.main.custom_view_search_bar.*
import kotlinx.android.synthetic.main.item_layout_search_result.*
import kotlinx.android.synthetic.main.item_layout_user_details.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SearchActivity : AppCompatActivity(), SearchView<SearchResult>,
    InternetReceiver.OnNetworkStatusReceiver {

    /*define variables here*/
    private var TAG = SearchActivity::class.java.canonicalName
    private lateinit var mSearchResult: SearchResult
    private lateinit var mInternetReceiver: InternetReceiver
    private lateinit var mFilter: IntentFilter
    lateinit var mBinding: ActivityMainBinding

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        init()
    }

    /* init view related logic here*/
    @RequiresApi(Build.VERSION_CODES.M)
    override fun init() {
        setSupportActionBar(toolbar)
        mInternetReceiver = InternetReceiver(this)
        registerReceiver()
        // Handle Imei option event
        edtSearch.setOnEditorActionListener(
            OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                // check string valid or not
                    if (ValidationUtils.textValidOrNot(edtSearch.text.toString())) {
                        callSearchAPI(edtSearch.text.toString())
                    } else {
                        Utils.displayToast(
                            this@SearchActivity,
                            getString(R.string.validation_search)
                        )
                    }
                false
            })
        // Handle drawable click event
        edtSearch.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= edtSearch.getRight() - edtSearch.getCompoundDrawables()
                        .get(DRAWABLE_RIGHT).getBounds().width()
                ) {
                    edtSearch.setText("")
                    // your action here
                    return@OnTouchListener true
                }
            }
            false
        })
        rlSearchResult.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                // Open details page of user1r4t
                openDetailsPage()
            }
        }
        )
    }

    /*Register internet receiver*/
    private fun registerReceiver() {
        mFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(mInternetReceiver, mFilter);
    }

    override fun onResume() {
        super.onResume()
        registerReceiver()
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mInternetReceiver);
    }

    /*Used for show progress*/
    override fun showProgress() {
        rlProgressBarView.visibility = View.VISIBLE
    }

    /*Used for hide progressbar*/
    override fun hideProgress() {
        rlProgressBarView.visibility = View.GONE
    }

    /*Used for set search result data*/
    override fun setData(datas: SearchResult) {
        if (getSupportActionBar() != null) {
            getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar()?.setDisplayShowHomeEnabled(true);
        }
        rlSearchBar.visibility = View.GONE
        mSearchResult = datas
        rlSearchResult.visibility = View.VISIBLE
        txtDescription.setText(datas.bio)
        txtLogin.setText(datas.login)
        txtName.setText(datas.name)
        txtFollowings.setText("" + datas.following)
        txtFollowers.setText("" + datas.followers)
        txtPublicCount.setText("" + datas.public_repos)
        Glide.with(this)
            .load(datas.avatar_url)
            .centerCrop()
            .placeholder(R.drawable.ic_github)
            .error(R.drawable.ic_github)
            .fallback(R.drawable.ic_github)
            .into(imgUser)
        imgUser.setOnClickListener(View.OnClickListener {
            Utils.loadURLinBrowser(this@SearchActivity, datas.html_url)
        })
    }

    /*Used for call search web call */
    @RequiresApi(Build.VERSION_CODES.M)
    override fun callSearchAPI(name: String) {
        if (Utils.isOnline(this)) {
            showProgress()
            val apiInterface = RetrofitClient.getClient().create(APIInterface::class.java)
            val call = apiInterface.getUserDetails(name)
            call.enqueue(object : Callback<SearchResult> {
                /*handle failure response*/
                override fun onFailure(call: Call<SearchResult>, t: Throwable) {
                    Utils.displayToast(
                        this@SearchActivity,
                        getString(R.string.something_went_wrong)
                    )
                    this@SearchActivity.hideProgress()
                }

                /*handle success response*/
                override fun onResponse(
                    call: Call<SearchResult>,
                    response: Response<SearchResult>
                ) {
                    // check data valid or not
                    if (response.body() != null) {
                        Utils.hideKeyboardFrom(this@SearchActivity, edtSearch)
                        setData(response.body()!!)
                    } else {
                        Utils.displayToast(
                            this@SearchActivity,
                            getString(R.string.something_went_wrong)
                        )
                    }
                    this@SearchActivity.hideProgress()
                }
            })
        }
    }

    /*Used for open details page of search result*/
    override fun openDetailsPage() {
        val gson = Gson()
        val json: String = gson.toJson(mSearchResult)
        val intent = Intent(this, UserDetailsActivity::class.java).apply {
            putExtra("data", json)
        }
        startActivity(intent)
    }

    /*Handle on back pressed listener*/
    override fun onBackPressed() {
        if (rlSearchResult.visibility == View.VISIBLE) {
            rlSearchResult.visibility = View.GONE
            rlSearchBar.visibility = View.VISIBLE
            if (getSupportActionBar() != null) {
                getSupportActionBar()?.setDisplayHomeAsUpEnabled(false);
                getSupportActionBar()?.setDisplayShowHomeEnabled(false);
            }
        } else {
            super.onBackPressed()
        }
    }

    /*Internet Connection Listener*/
    override fun networkStatusReceiver(data: Boolean) {
        if (data) {
            rlInternetConnection.visibility = View.GONE
        } else {
            rlInternetConnection.visibility = View.VISIBLE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}