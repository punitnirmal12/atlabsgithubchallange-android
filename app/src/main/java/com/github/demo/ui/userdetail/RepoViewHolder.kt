package com.github.demo.ui.userdetail

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.github.demo.R
import com.github.demo.base.BaseViewHolder
import com.github.demo.utils.Utils
import com.github.demo.utils.ValidationUtils

class RepoViewHolder(view: View) : BaseViewHolder(view) {

    var mTxtName: TextView = view.findViewById(R.id.txtName)
    var mTxtDescription: TextView = view.findViewById(R.id.txtDescription)
    var mTxtLanguage: TextView = view.findViewById(R.id.txtLanguagesAt)
    var mTxtCreatedAt: TextView = view.findViewById(R.id.txtCreatedAt)
    var mTxtPushedAt: TextView = view.findViewById(R.id.txtUpdatedAt)
    var mTxtIssueCount: TextView = view.findViewById(R.id.txtOpenIssueCount)
    var mImgGithub: ImageView = view.findViewById(R.id.imgGithub)

    fun setName(name: String) {
        if (ValidationUtils.textValidOrNot(name))
            mTxtName.setText(name)
    }

    fun setDescription(description: String) {
        if (ValidationUtils.textValidOrNot(description))
            mTxtDescription.setText(description)
    }

    fun setLanguage(language: String) {
        if (ValidationUtils.textValidOrNot(language))
            mTxtLanguage.setText(language)
    }

    fun setCreatedAt(createdAt: String) {
        if (ValidationUtils.textValidOrNot(createdAt))
            mTxtCreatedAt.setText((createdAt))
    }

    fun setPushedAt(pushedAt: String) {
        if (ValidationUtils.textValidOrNot(pushedAt))
            mTxtPushedAt.setText((pushedAt))
    }

    fun setIssueCount(pushedAt: Int) {
        mTxtIssueCount.setText("" + pushedAt)
    }
}