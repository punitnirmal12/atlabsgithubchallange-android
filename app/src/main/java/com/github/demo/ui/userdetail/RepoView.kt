package com.github.demo.ui.userdetail

import com.github.demo.base.BaseView

interface RepoView<T> : BaseView<T> {
    /*display listing data on list view*/
    fun callRepoListWebservice(name: String)
}