package com.github.demo.ui.userdetail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.demo.R
import com.github.demo.base.BaseRecyclerViewAdapter
import com.github.demo.model.UserRepoDetails
import com.github.demo.utils.Utils

class RepoAdapter(private var mContext: Context, private var mList: List<UserRepoDetails>) :
    BaseRecyclerViewAdapter<RepoViewHolder>() {

    private var TAG = RepoAdapter::class.java.canonicalName

    override fun bindHolder(holder: RepoViewHolder, position: Int, payloads: MutableList<Any>?) {
        holder.setName(mList.get(position).name)
        mList.get(position).description?.let { holder.setDescription(it) }
        mList.get(position).language?.let { holder.setLanguage(it) }
        mList.get(position).created_at?.let { holder.setCreatedAt(it) }
        mList.get(position).pushed_at?.let { holder.setPushedAt(it) }
        holder.setIssueCount(mList.get(position).open_issues_count)
        holder.mImgGithub.setTag(position)
        holder.mImgGithub.setOnClickListener(View.OnClickListener {
            val i = it.getTag()
            Utils.loadURLinBrowser(mContext, mList.get(i as Int).html_url)
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val itemView = LayoutInflater.from(mContext)
            .inflate(R.layout.item_layout_github_repo_link, parent, false)
        return RepoViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun itemClicked(id: Int, position: Int, bundle: Bundle?) {
    }
}